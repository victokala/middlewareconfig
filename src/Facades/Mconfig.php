<?php

namespace Mainone\MiddlewareConfig\Facades;

use Illuminate\Support\Facades\Facade;

class Mconfig extends Facade{

    protected static function getFacadeAccessor()
    {
        return 'mconfig';
    }
}