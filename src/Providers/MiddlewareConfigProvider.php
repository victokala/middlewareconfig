<?php

namespace Mainone\MiddlewareConfig\Providers;

use Illuminate\Support\ServiceProvider;
use Mainone\MiddlewareConfig\MiddlewareConfig;

class MiddlewareConfigProvider extends ServiceProvider{

    public function boot(){
    }

    public function register()
    {
        $this->app->bind('mconfig', function(){
            return new MiddlewareConfig();
        });
    }
}