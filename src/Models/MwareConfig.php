<?php

namespace Mainone\MiddlewareConfig\Models;

use Illuminate\Database\Eloquent\Model;

class MwareConfig extends Model {
    
    protected $fillable = ['name', 'value'];
}