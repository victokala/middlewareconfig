<?php

namespace Mainone\MiddlewareConfig;

use Mainone\MiddlewareConfig\Models\MwareConfig;

class MiddlewareConfig {

    public function getConfig($name){
        $config = MwareConfig::where('name', $name)->first();
        if(is_null($config)){
            return null;
        }
        return $config->value;
    }

    public function setConfig($name, $value){
        $config = MwareConfig::where('name', $name)->first();
        if($config){
            $config->value = $value;
        }else{
            $config = new MwareConfig([
                'name' => $name,
                'value' => $value
            ]);
        }
        return $config->save();
    }
}